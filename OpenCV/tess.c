#include <tesseract/baseapi.h>
#include <tesseract/strngs.h>
#include<stdio.h>
int main(int argc, char** argv) {
  const char* lang = "eng";
  tesseract::TessBaseAPI tess;
  tess.Init(NULL, lang, tesseract::OEM_DEFAULT);
  tess.SetPageSegMode(tesseract::PSM_SINGLE_BLOCK);
  FILE* fin = fopen(argv[1], "rb");
  if(fin== NULL) {
    printf("Cannot open %s\n", argv[1]);
    return -1;
  }
  fclose(fin);
  STRING text;
  if(!tess.ProcessPages(argv[1], NULL, 0, &text)) {
    printf("Error during processing.\n");
    return -1;
  }
  else
    printf(text.string());
  return 0;
}
