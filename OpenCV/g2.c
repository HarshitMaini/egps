#include<highgui.h>
#include<cv.h> 
#include<stdio.h>

int main(int argc, char **argv) {
  int a= -1;
  int *p;
  p= &a;
  IplImage* orig = cvLoadImage(argv[1],1);
  if (!orig) {
    return -1;
  }
  
  //printf("Orig dimensions: %dx%d\n", orig->width, orig->height);
  cvSetImageROI(orig, cvRect(180, 115, 55, 15));
  IplImage *tmp = cvCreateImage(cvGetSize(orig), orig->depth, orig->nChannels);
  cvCopy(orig, tmp, NULL);
  cvResetImageROI(orig);
  orig = cvCloneImage(tmp);
  //printf("Orig dimensions after crop: %dx%d\n", orig->width, orig->height);
  cvNamedWindow( "CROP",1  );
  cvShowImage( "CROP", orig);
  cvSaveImage("CROP.png", tmp, p);
  cvWaitKey(0);
  cvDestroyWindow("CROP");
  return 0;
}
