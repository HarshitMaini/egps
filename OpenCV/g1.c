#include<highgui.h>
#include<cv.h>

IplImage* img_resize(IplImage* src_img, int new_width,int new_height) {
  IplImage* des_img;
  des_img=cvCreateImage(cvSize(new_width,new_height),src_img->depth,src_img->nChannels);
  cvResize(src_img,des_img,CV_INTER_LINEAR);
  return des_img;
} 


int main(int argc, char **argv) {
  int a= -1;
  int *p;
  p= &a;
  IplImage* img= cvLoadImage(argv[1],1);
  cvNamedWindow("ID_CARD_SCAN", 1);
  IplImage* newImage=img_resize(img, 350,210);
  IplImage *grayScaleImage = cvCreateImage(cvSize(350,210),8,1);
  cvShowImage("ID_CARD_SCAN", newImage);
  cvSaveImage("MODIFIED_ID_IMAGE.jpg", newImage, p);
  cvWaitKey(0);
  //cvDestroyImage("ID_CARD_SCAN");
  cvReleaseImage(&img);
  return 0;
}
