import java.sql.*;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.util.Calendar;

public class MainClass
{
  static
  {
    System.loadLibrary("NativeLib");
  }
  public static void main(String[] args)
  {
    NativeMethods nm= new NativeMethods();
    String id=nm.readText();
    id=id.trim();
    try
    {            
      Connection conn= DriverManager.getConnection("jdbc:mysql://localhost:3306/egps", "root", "root");
      String ss="insert into checkouts(strStudentID,dateCheckout,strDepartment) values (?,?,?)";
      PreparedStatement preparedStatement= conn.prepareStatement(ss);
      DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
      Calendar cal= Calendar.getInstance();
      String dateTime= dateFormat.format(cal.getTime());
      String deptt= id.substring(2,4);
      preparedStatement.setString(1, id);
      preparedStatement.setString(2, dateTime);
      preparedStatement.setString(3, deptt);
      preparedStatement.executeUpdate();
    } 
    catch(Exception e)
    {
      System.out.println(e);
    }
  }
}
