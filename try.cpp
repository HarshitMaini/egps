#include<iostream>
#include<opencv2/core/core.hpp>
#include<opencv2/highgui/highgui.hpp>
using namespace cv;
using namespace std;
int main(int argc, char* argv[])
{
  try
  {
    Mat image= imread("img.jpg");
    namedWindow("My Image");
    flip(image, image, 1);
    imshow("My Image", image);
    waitKey(5000);
  }
  catch(Exception e)
  {
    cout<<"Exception.";
  }
  return 0;
}
