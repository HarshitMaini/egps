#include<tesseract/baseapi.h>
#include<leptonica/allheaders.h>
#include<iostream>
#include<string>
#include<opencv/cv.h>
#include<opencv/highgui.h>
#include "NativeMethods.h"
using namespace std;
using namespace tesseract;
using namespace cv;
JNIEXPORT jstring JNICALL Java_NativeMethods_readText(JNIEnv* env, jobject thisObj) {
    //tesseract::TessBaseAPI tess;
    TessBaseAPI tess;
    tess.Init(NULL, "eng", tesseract::OEM_DEFAULT);
    Mat image = imread("c3.png");
    tess.SetImage((uchar*)image.data, image.size().width, image.size().height, image.channels(), image.step1());
    tess.Recognize(0);
    char* out = tess.GetUTF8Text();
    int len=strlen(out);
    jchar* aa = new jchar[len];
    for (int i=0; i<len; i++)
		aa[i] = out[i];
    jstring js= env->NewString(aa,len);
    //cout << out;
    return js;
}
