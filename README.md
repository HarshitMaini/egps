The Project consist of 2 parts:
Hardware part, and
software part.

software part accepts a image of student ID-card, pre-process it, extracts student ID number and stores it in database.
Image Preprocessing is done using opencv.
OCR is accomplished by tesseract lib.
Database operations are programmed in java.

1. A Java Program(MainClass.java) accepts an image and saves it in current 
   directory.
2. MainClass.java calls b.cpp using JNI.
3. b.cpp performs OCR and return the student ID String to MainClass.java.
4. MainClass.java adds the student ID to the database.

NativeMethods.java and NativeLib.so are written for JNI.

It also consist of a front-end to the database. It is developed using web technologies with php for server-side scripting.

###########################
###########################

Hardware Part


